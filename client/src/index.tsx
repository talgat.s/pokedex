import React from "react";
import ReactDOM from "react-dom";
import {
    ApolloProvider,
    ApolloClient,
    HttpLink,
    InMemoryCache,
} from "@apollo/client";
import { BrowserRouter as Router } from "react-router-dom";
import "./index.css";

import * as serviceWorker from "./serviceWorker";

import App from "./components/App";

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: new HttpLink({}),
});

ReactDOM.render(
    <React.StrictMode>
        <ApolloProvider client={client}>
            <Router>
                <App />
            </Router>
        </ApolloProvider>
    </React.StrictMode>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
