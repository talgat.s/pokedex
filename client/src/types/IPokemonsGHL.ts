import { IPokemon } from "./IPokemon";

export interface IPokemonsGHL {
	count: number;
	results: IPokemon[];
}
