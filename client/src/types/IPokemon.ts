interface IStat {
	base_stat: number;
	stat: {
		name: string;
	};
}

interface IType {
	type: {
		name: string;
	};
}

export interface IPokemon {
	id: string;
	name: string;
	sprites: {
		front_default: string;
	};
	stats: IStat[];
	types: IType[];
}
