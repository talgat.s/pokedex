import React from "react";
import { useQuery, gql } from "@apollo/client";
import { List, Card, Avatar, Tag, Pagination } from "antd/es";
import styles from "./PokemonsList.module.css";

import { DEFAULT_SIZES } from "constants/index";

import GraphqlContainer from "components/GraphqlContainer";

import { IPokemonsGHL } from "types/";

interface IPokemonsData {
    pokemons: IPokemonsGHL;
}

const POKEMONS = gql`
    query Pokemons($limit: Int!, $offset: Int) {
        pokemons(limit: $limit, offset: $offset) {
            count
            results {
                id
                name
                sprites {
                    front_default
                }
                stats {
                    base_stat
                    stat {
                        name
                    }
                }
                types {
                    type {
                        name
                    }
                }
            }
        }
    }
`;

interface PokemonsListProps {
    page: number;
    size: number;
    onPageChange: (page: number, size?: number) => void;
    onShowSizeChange: (current: number, size: number) => void;
}
const PokemonsList: React.FC<PokemonsListProps> = ({
    page,
    size,
    onPageChange,
    onShowSizeChange,
}) => {
    const { loading, error, data } = useQuery<IPokemonsData>(POKEMONS, {
        variables: { limit: size, offset: (page - 1) * size },
    });

    return (
        <GraphqlContainer loading={loading} error={error}>
            <List
                className={styles.PokemonsList}
                grid={{
                    gutter: 16,
                    xs: 1,
                    sm: 2,
                    md: 3,
                    lg: 4,
                    xl: 4,
                    xxl: 5,
                }}
                dataSource={data?.pokemons.results}
                rowKey="id"
                renderItem={(pokemon) => (
                    <List.Item>
                        <Card
                            title={
                                <div className={styles.cardTitle}>
                                    <span>{pokemon.name}</span>
                                    <Avatar
                                        className={styles.avatar}
                                        src={pokemon.sprites.front_default}
                                    />
                                </div>
                            }
                        >
                            <List
                                dataSource={pokemon.stats}
                                renderItem={({
                                    base_stat: baseStat,
                                    stat: { name },
                                }) => (
                                    <List.Item>
                                        <div className={styles.stat}>
                                            <span>{name}</span>
                                            <span>{baseStat}</span>
                                        </div>
                                    </List.Item>
                                )}
                            />
                            <div>
                                {pokemon.types.map(({ type: { name } }) => (
                                    <Tag key={name} color="#108ee9">
                                        {name}
                                    </Tag>
                                ))}
                            </div>
                        </Card>
                    </List.Item>
                )}
            />
            <nav className={styles.pagination}>
                <Pagination
                    current={page}
                    pageSize={size}
                    pageSizeOptions={DEFAULT_SIZES.map(String)}
                    total={data?.pokemons.count}
                    onChange={onPageChange}
                    onShowSizeChange={onShowSizeChange}
                />
            </nav>
        </GraphqlContainer>
    );
};

export default PokemonsList;
