import React, { Fragment } from "react";
import { ApolloError } from "@apollo/client";
import { Spin } from "antd/es";
import { LoadingOutlined } from "@ant-design/icons";
import styles from "./GraphqlContainer.module.css";

const AntIcon = <LoadingOutlined className={styles.loadingIcon} spin />;

interface GraphqlContainerProps {
    loading: boolean;
    error?: ApolloError;
}
const GraphqlContainer: React.FC<GraphqlContainerProps> = ({
    children,
    loading,
    error,
}) => {
    if (loading) {
        return (
            <div className={styles.loading}>
                <Spin indicator={AntIcon} />
            </div>
        );
    } else if (error) {
        return <div>error</div>;
    } else {
        return <Fragment>{children}</Fragment>;
    }
};

export default GraphqlContainer;
