import React from "react";
import { RouteComponentProps, Redirect } from "react-router-dom";

import {
    PAGE_KEY,
    SIZE_KEY,
    DEFAULT_PAGE,
    DEFAULT_SIZES,
} from "constants/index";

import PokemonsList from "components/PokemonsList";

interface PokemonsProps extends RouteComponentProps {}
const Pokemons: React.FC<PokemonsProps> = ({ location, history }) => {
    const searchParams = new URLSearchParams(location.search);
    const page = Number(searchParams.get(PAGE_KEY));
    const size = Number(searchParams.get(SIZE_KEY));
    if (page && !size) {
        return (
            <Redirect
                to={{
                    pathname: "/pokemons",
                    search: `?${PAGE_KEY}=${page}&${SIZE_KEY}=${DEFAULT_SIZES[0]}`,
                }}
            />
        );
    } else if (!page && size) {
        return (
            <Redirect
                to={{
                    pathname: "/pokemons",
                    search: `?${PAGE_KEY}=${DEFAULT_PAGE}&${SIZE_KEY}=${size}`,
                }}
            />
        );
    } else if (!page && !size) {
        return (
            <Redirect
                to={{
                    pathname: "/pokemons",
                    search: `?${PAGE_KEY}=${DEFAULT_PAGE}&${SIZE_KEY}=${DEFAULT_SIZES[0]}`,
                }}
            />
        );
    }

    function onPageChange(page: number, size?: number) {
        history.push({
            pathname: "/pokemons",
            search: `?${PAGE_KEY}=${page}&${SIZE_KEY}=${size}`,
        });
    }

    function onShowSizeChange(_: number, size: number) {
        history.push({
            pathname: "/pokemons",
            search: `?${PAGE_KEY}=${DEFAULT_PAGE}&${SIZE_KEY}=${size}`,
        });
    }

    return (
        <PokemonsList
            page={page}
            size={size}
            onPageChange={onPageChange}
            onShowSizeChange={onShowSizeChange}
        />
    );
};

export default Pokemons;
