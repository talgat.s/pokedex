import React from "react";
import {
    Switch,
    Redirect,
    Route,
    RouteComponentProps,
    withRouter,
} from "react-router-dom";
import { Layout } from "antd/es/";
import styles from "./App.module.css";

import Pokemons from "components/Pokemons";

const { Content } = Layout;

interface AppProps extends RouteComponentProps {}
const App: React.FC<AppProps> = () => {
    return (
        <Layout className={styles.App}>
            <Content className={styles.main}>
                <Switch>
                    <Redirect exact from="/" to="/pokemons" />
                    <Route exact path="/pokemons" component={Pokemons} />
                    <Route render={() => <div>404</div>} />
                </Switch>
            </Content>
        </Layout>
    );
};

export default withRouter(App);
