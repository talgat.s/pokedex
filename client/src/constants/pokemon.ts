export const PAGE_KEY = "page";
export const SIZE_KEY = "size";
export const DEFAULT_PAGE = 1;
export const DEFAULT_SIZES = [12, 24, 36];
