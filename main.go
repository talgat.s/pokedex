package main

import (
	"encoding/json"
	log "gopkg.in/inconshreveable/log15.v2"
	"net/http"
	"os"
	"time"

	"gitlab.com/talgat.s/pokedex/env"
	"gitlab.com/talgat.s/pokedex/graphql"
)

type queryBody struct {
	OperationName string                 `json:"operationName"`
	Query         string                 `json:"query"`
	Variables     map[string]interface{} `json:"variables"`
}

func routes() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/graphql", func(w http.ResponseWriter, r *http.Request) {
		if r.Body == nil {
			http.Error(w, "No query data", 400)
			return
		}

		var qb queryBody
		err := json.NewDecoder(r.Body).Decode(&qb)
		if err != nil {
			http.Error(w, "Error parsing JSON request body", 400)
			return
		}

		result, err := graphql.Query(qb.OperationName, qb.Query, qb.Variables)
		if err != nil {
			http.Error(w, "Error in query", 400)
			return
		}

		json.NewEncoder(w).Encode(result)
	})
	if env.IsNot(env.LOCAL_PROD, env.REMOTE_PROD) {
		mux.Handle(
			"/graphiql/",
			http.StripPrefix("/graphiql/", graphql.GraphiQLHandler()),
		)
	}
	mux.Handle("/", http.FileServer(http.Dir("./client/build/")))

	return mux
}

func main() {
	// setup env
	env.Config()

	// application
	port := os.Getenv("PORT")
	log.Info("Starting APP on localhost", "PORT", port)
	addr := ":" + port
	srv := &http.Server{
		Addr:         addr,
		Handler:      routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	err := srv.ListenAndServeTLS("./tls/cert.pem", "./tls/key.pem")
	log.Crit("Unable to start web server at", err)
}
