package main

import (
	log "gopkg.in/inconshreveable/log15.v2"
	"os/exec"

	"gitlab.com/talgat.s/pokedex/graphql"
)

func main() {
	log.Info("Started building graphiql")
	cmd := exec.Command("npm", "run", "build")
	cmd.Dir = graphql.GraphiqlPath
	if err := cmd.Run(); err != nil {
		log.Crit("Error in building graphiql", "error", cmd.String())
		return
	}
	log.Info("Graphiql was built successfuly")
}
