package main

import (
	log "gopkg.in/inconshreveable/log15.v2"
	"os/exec"
)

func main() {
	log.Info("Started building client")
	cmd := exec.Command("npm", "run", "build")
	cmd.Dir = "./client/"
	if err := cmd.Run(); err != nil {
		log.Crit("Error in building client", "error", cmd.String())
		return
	}
	log.Info("Client was built successfuly")
}
