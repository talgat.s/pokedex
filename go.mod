module gitlab.com/talgat.s/pokedex

go 1.14

require (
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/graphql-go/graphql v0.7.9
	github.com/joho/godotenv v1.3.0
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mtslzr/pokeapi-go v1.3.2
	gopkg.in/inconshreveable/log15.v2 v2.0.0-20200109203555-b30bc20e4fd1
)
