package graphql

import (
	"github.com/graphql-go/graphql"
	log "gopkg.in/inconshreveable/log15.v2"
	"net/http"
	"path/filepath"
)

const GraphiqlPath = "./graphiql-client/"

func newSchema() (graphql.Schema, error) {
	return graphql.NewSchema(
		graphql.SchemaConfig{
			Query: queryType,
		},
	)
}

func Query(name, query string, vars map[string]interface{}) (*graphql.Result, error) {
	schema, err := newSchema()
	if err != nil {
		return nil, err
	}

	result := graphql.Do(graphql.Params{
		Schema:         schema,
		OperationName:  name,
		RequestString:  query,
		VariableValues: vars,
	})
	if len(result.Errors) > 0 {
		log.Warn("wrong result", "errors", result.Errors)
	}
	return result, nil
}

func GraphiQLHandler() http.Handler {
	return http.FileServer(http.Dir(filepath.Join(GraphiqlPath, "/build/")))
}
