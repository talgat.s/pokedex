package graphql

import (
	"fmt"
	"github.com/graphql-go/graphql"
	pokeapi "github.com/mtslzr/pokeapi-go"
	"github.com/mtslzr/pokeapi-go/structs"
	log "gopkg.in/inconshreveable/log15.v2"
)

var pokemonsResourceGHL = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonsResources",
		Fields: graphql.Fields{
			"count": &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"results": &graphql.Field{
				Type: graphql.NewList(pokemonResultsItemGHL),
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					parent := p.Source.(structs.Resource)
					num := len(parent.Results)
					result := []structs.Pokemon{}

					ch := make(chan structs.Pokemon)
					for i := 0; i < num; i++ {
						go func(ch chan<- structs.Pokemon, name string, i int) {
							pokemon, err := pokeapi.Pokemon(name)
							if err != nil {
								log.Crit("Pokedex api issues", "error", err)
								close(ch)
							}
							ch <- pokemon
						}(ch, parent.Results[i].Name, i)
					}
					for i := 0; i < num; i++ {
						pokemon, ok := <-ch
						if !ok {
							return nil, fmt.Errorf("Pokedex api issues")
						}
						result = append(result, pokemon)
					}
					close(ch)

					return result, nil
				},
			},
		},
	},
)

var pokemonResultsItemGHL = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonResultItem",
		Fields: graphql.Fields{
			"abilities":       &graphql.Field{Type: graphql.NewList(pokemonAbilityItem)},
			"base_experience": &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"forms":           &graphql.Field{Type: graphql.NewList(itemField)},
			"height":          &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"id":              &graphql.Field{Type: graphql.NewNonNull(graphql.ID)},
			"name":            &graphql.Field{Type: graphql.NewNonNull(graphql.String)},
			"species":         &graphql.Field{Type: graphql.NewList(itemField)},
			"sprites":         &graphql.Field{Type: pokemonSprites},
			"stats":           &graphql.Field{Type: graphql.NewList(pokemonStatItem)},
			"types":           &graphql.Field{Type: graphql.NewList(pokemonTypesItem)},
			"weight":          &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
		},
	},
)

var itemField = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonAbility",
		Fields: graphql.Fields{
			"name": &graphql.Field{Type: graphql.NewNonNull(graphql.String)},
			"url":  &graphql.Field{Type: graphql.NewNonNull(graphql.String)},
		},
	},
)

var pokemonAbilityItem = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonAbilityItem",
		Fields: graphql.Fields{
			"slot":      &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"is_hidden": &graphql.Field{Type: graphql.NewNonNull(graphql.Boolean)},
			"ability":   &graphql.Field{Type: graphql.NewNonNull(itemField)},
		},
	},
)

var pokemonSprites = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonSprites",
		Fields: graphql.Fields{
			"back_default":       &graphql.Field{Type: graphql.String},
			"back_female":        &graphql.Field{Type: graphql.String},
			"back_shiny":         &graphql.Field{Type: graphql.String},
			"back_shiny_female":  &graphql.Field{Type: graphql.String},
			"front_default":      &graphql.Field{Type: graphql.String},
			"front_female":       &graphql.Field{Type: graphql.String},
			"front_shiny":        &graphql.Field{Type: graphql.String},
			"front_shiny_female": &graphql.Field{Type: graphql.String},
		},
	},
)

var pokemonStatItem = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonStatItem",
		Fields: graphql.Fields{
			"base_stat": &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"effort":    &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"stat":      &graphql.Field{Type: graphql.NewNonNull(itemField)},
		},
	},
)

var pokemonTypesItem = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "PokemonTypesItem",
		Fields: graphql.Fields{
			"slot": &graphql.Field{Type: graphql.NewNonNull(graphql.Int)},
			"type": &graphql.Field{Type: graphql.NewNonNull(itemField)},
		},
	},
)
