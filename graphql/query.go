package graphql

import (
	"errors"
	"github.com/graphql-go/graphql"
	pokeapi "github.com/mtslzr/pokeapi-go"
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"pokemons": &graphql.Field{
				Type: pokemonsResourceGHL,
				Args: graphql.FieldConfigArgument{
					"limit": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.Int),
					},
					"offset": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
					"type": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					limit, isOK := p.Args["limit"].(int)
					if !isOK || limit < 1 {
						return nil, errors.New("Wrong limit argument")
					}

					offset, isOK := p.Args["offset"].(int)
					if !isOK {
						offset = 0
					}
					if offset < 0 {
						return nil, errors.New("Wrong offset argument")
					}

					pokemonType, isOK := p.Args["type"].(string)
					if !isOK {
						pokemonType = ""
					}

					if pokemonType == "" {
						r, _ := pokeapi.Resource("pokemon", offset, limit)
						return r, nil
					}

					return nil, errors.New("ERROR")
				},
			},
		},
	},
)
